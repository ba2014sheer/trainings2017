#include "Account.hpp"

#include <iostream>

Account::Account(int balance)
{
    setBalance(balance);
}

void
Account::setBalance(int balance)
{
    if (balance < 0) {
        std::cout << "Error 1. Invalid Balance. Resetting to 0 ..." << std::endl;
        balance_ = 0; 
        return;
    }

    balance_ = balance;
}

int
Account::getBalance()
{
    return balance_;
}

void
Account::credit(int creditSum)
{
    balance_ = balance_ + creditSum;
}

void
Account::debit(int debitSum)
{
    if (debitSum > balance_) {
        std::cout << "Error 2. Debit sum is greater than account balance." << std::endl;
        return;
    }

    balance_ = balance_ - debitSum;
}

