#include "Date.hpp"
#include <iostream>

Date::Date(int day, int month, int year)
{
    setDay(day);
    setMonth(month);
    setYear(year);
}

void
Date::setDay(int day)
{
    day_ = day;
}

int
Date::getDay()
{
    return day_;
}

void
Date::setMonth(int month)
{
    month_ = month;
    if (month < 1) {
        std::cout << "Info 1: Invalid month. Reset to 1. " << std::endl;
        month_ = 1;
    }

    if (month > 12) {
        std::cout << "Info 1: Invalid month. Reset to 1. " << std::endl;
        month_ = 1;
    }
}

int
Date::getMonth()
{
    return month_;
}

void
Date::setYear(int year)
{
    year_ = year;
}

int
Date::getYear()
{
    return year_;
}

void
Date::displayDate()
{
    std::cout << getDay() << "/" << getMonth() << "/" << getYear() << std::endl;
}

