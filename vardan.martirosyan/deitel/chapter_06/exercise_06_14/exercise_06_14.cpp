#include <iostream>
#include <cmath>

float roundToInteger (const float number);
float roundToTenths (const float number);
float roundToHundreths (const float number);
float roundToTousanths (const float number);

int
main()
{
    std::cout << "Test number is 2.56338 \nRound to integer is "    << roundToInteger(2.56338)   << std::endl;
    std::cout << "Round to tenths is "     << roundToTenths(2.56338)    << std::endl;
    std::cout << "Round to hundreaths is " << roundToHundreths(2.56338) << std::endl;
    std::cout << "Round to tousanths is "  << roundToTousanths(2.56338) << std::endl;

    return 0;
}

float
roundToInteger(const float number)
{
    const float calculation = std::floor(number * 10 + 0.5) / 10;
    return calculation;
}

float
roundToTenths(const float number)
{
    const float calculation = std::floor(number * 100 + 0.5) / 100;
    return calculation;
}

float
roundToHundreths(const float number)
{
    const float calculation = std::floor(number * 1000 + 0.5) / 1000;
    return calculation;
}

float
roundToTousanths(const float number)
{
    const float calculation = std::floor(number * 10000 + 0.5) / 10000;
    return calculation;
}


