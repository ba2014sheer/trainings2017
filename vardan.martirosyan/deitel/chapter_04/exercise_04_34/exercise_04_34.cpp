#include <iostream>

int
main()
{
    char subExercise;
    std::cout << "a) Encoding\nb) Decoding\nChoose sub-exercise: ";
    std::cin  >> subExercise;

    if ('a' == subExercise) {
        int number;
        std::cout << "Enter number: ";
        std::cin  >> number;

        if (number < 1000) {
            std::cerr << "Error 1: The number must be four digits." << std::endl;
            return 1;
        }
        if (number > 9999) {
            std::cerr << "Error 1: The number must be four digits." << std::endl;
            return 1;
        }

        int digit1 = (number / 1000 + 7) % 10;
        int digit2 = (number / 100 + 7) % 10;
        int digit3 = (number / 10 + 7) % 10;
        int digit4 = (number / 1 + 7) % 10;

        std::cout << "Your encoded number is " << digit3 << digit4 << digit1 << digit2 << std::endl;
        return 0;
    }

    if ('b' == subExercise) {
        int number;
        std::cout << "Enter number: ";
        std::cin  >> number;

        if (number < 0) {
            std::cerr << "Error 1: The number must be four digits." << std::endl;
            return 1;
        }
        if (number > 9999) {
            std::cerr << "Error 1: The number must be four digits." << std::endl;
            return 1;
        }

        int digit1 = (number / 1000 + 3) % 10;
        int digit2 = (number / 100 + 3) % 10;
        int digit3 = (number / 10 + 3) % 10;
        int digit4 = (number / 1 + 3) % 10;    

        std::cout << "Your decodeinf number is " << digit3 << digit4 << digit1 << digit2 << std::endl;
        return 0;
    }
    std::cerr << "Error 2: Invalid choice." << std::endl;
    return 2;
}

