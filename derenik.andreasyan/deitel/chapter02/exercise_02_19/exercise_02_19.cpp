#include <iostream>

int
main()
{
    int number1;
    int number2;
    int number3;

    std::cout << "Input three different integers: ";
    std::cin >> number1 >> number2 >> number3;

    if (number1 == number2) {
        std::cout << "Error 1: Not different integers\n";
	return 1;
    } 
    if (number2 == number3) {
        std::cout << "Error 1: Not different integers\n";
        return 1;
    }
    if (number1 == number3) {
        std::cout << "Error 1: Not different integers\n";
	return 1;
    }


    std::cout << "Sum is " << (number1 + number2 + number3) << std::endl;
    std::cout << "Average is " << (number1 + number2 + number3) / 3 << std::endl;
    std::cout << "Product is " << (number1 * number2 * number3) << std::endl;

    int minimum = number1;
    int maximum = number1;

    if (minimum > number2) {
        minimum = number2;
    }
    if (minimum > number3) {
        minimum = number3;
    }
    
    std::cout << "Smallest is " << minimum << std::endl;

    if (maximum < number2) {
        maximum = number2;
    }
    if (maximum < number3) {
        maximum = number3;
    }

    std::cout << "Largest is " << maximum << std::endl;

    return 0;
}
