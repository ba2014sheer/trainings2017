#include <iostream>

int
main()
{
    std::cout << "N\t10*N\t100*N\t1000*N\n" << std::endl;

    int number = 1;

    while (number <= 5) {
        std::cout << number << "\t" << number * 10 << "\t" << number * 100 << "\t" << number * 1000 << "\n";
        ++number;
    }
    return 0;
}


