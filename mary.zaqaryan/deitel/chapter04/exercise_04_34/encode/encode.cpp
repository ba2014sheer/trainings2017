#include <iostream>

int 
main()
{
    int number;
    std::cout << "Enter a number to encode it: ";
    std::cin >> number;

    if (number < 1000) {
        std::cerr << "Error1: Enter only 4 digit number." << std::endl;
        return 1;
    }
    if (number > 9999) {
        std::cerr << "Error1: Enter only 4 digit number." << std::endl;
        return 1;
    }

    int digit1 = (number / 1000 + 7) % 10;
    int digit2 = (number / 100 + 7) % 10;
    int digit3 = (number / 10 + 7) % 10;
    int digit4 = (number + 7) % 10;

    int encodedNumber = digit3 * 1000 + digit4 * 100 + digit1 * 10 + digit2;

    std::cout << "Encoded number: " << encodedNumber << std::endl;
    return 0;
}
