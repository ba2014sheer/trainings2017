#include <iostream>

int
main()
{
    int number1;
    std::cout << "Enter number 1: ";
    std::cin >> number1;
    
    int number2;
    std::cout << "Enter number 2: ";
    std::cin >> number2;

    if (number1 > number2) {
        std::cout << number1 << " is larger\n";
        return 0;
    } 

    if (number1 < number2) {
        std::cout << number2 << " is larger\n";
        return 0;
    }

    std::cout << "These numbers are equal\n";
    return 0;
}
