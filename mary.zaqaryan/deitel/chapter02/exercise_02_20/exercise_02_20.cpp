#include <iostream>

int
main()
{
    int radius;
    std::cout << "Enter radius: ";
    std::cin >> radius;
    
    if (radius < 0) {
        std::cout << "Error 1: radius can't be negative!\n";
        return 1;
    }
    std::cout << "Diameter is " << 2 * radius << "\n";
    std::cout << "Circumference is " << 2 * 3.14159 * radius << "\n";
    std::cout << "Area is " << 3.14159 * radius * radius << "\n";
    return 0;
    
}
