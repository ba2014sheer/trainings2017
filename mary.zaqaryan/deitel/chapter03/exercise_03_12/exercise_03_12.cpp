#include "Account.hpp"
#include <iostream>

int
main()
{
    Account account1(30000);
    Account account2(0);

    std::cout << "The first account's balance is " << account1.getAccountBalance() << std::endl;

    int creditAmount = 2000;
    int debitAmount = 1100;

    std::cout << "The first account's balance after credit is " << account1.credit(creditAmount) << std::endl;
    std::cout << "The first account's balance after debit is " << account1.debit(debitAmount) << std::endl;

    std::cout << "The second account's balance is " << account2.getAccountBalance() << std::endl;

    creditAmount = 4000;
    debitAmount = 9500;

    std::cout << "The second account's balance after credit is " << account2.credit(creditAmount) << std::endl;
    std::cout << "The second account's balance after debit is " << account2.debit(debitAmount) << std::endl;


    return 0;
}


