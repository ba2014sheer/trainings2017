#include "Date.hpp"
#include <iostream>

int
main()
{
    Date date1(30, 4, 2015);
    Date date2(21, 13, 2018);

    date1.displayDate();
    date2.displayDate();
    return 0;
}


