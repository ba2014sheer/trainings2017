#include <iostream>
#include <unistd.h>
#include <cassert>

void selectionSort(int array[], const size_t arraySize, const size_t firstElementIndex = 0);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "This program shows how the selection sort works\n";
        std::cout << "Insert 10 elements of array\n";
    }
    size_t ARRAY_SIZE = 10;
    int array[ARRAY_SIZE];
    for (size_t index = 0; index < ARRAY_SIZE; ++index) {
        std::cin >> array[index];
    }
    std::cout << "array data in the original order.\n";
    for (size_t index = 0; index < ARRAY_SIZE; ++index) {
        std::cout << array[index] << " ";
    }
    selectionSort(array, ARRAY_SIZE);
    std::cout << "\narray data in ascending order.\n";
    for (size_t index = 0; index < ARRAY_SIZE; ++index) {
        std::cout << array[index] << " ";
    } 
    std::cout << std::endl; 

    return 0;
}

void 
selectionSort(int array[], const size_t arraySize, const size_t firstElementIndex)
{
    assert((arraySize > 0) && (firstElementIndex < arraySize));
    if (firstElementIndex == arraySize - 1) {
        return;
    } 
    int minimum = array[firstElementIndex];
    size_t indexOfMinimum = firstElementIndex;
    for (size_t index = firstElementIndex + 1; index < arraySize; ++index) {
        if (minimum > array[index]) {
            minimum = array[index];
            indexOfMinimum = index;
        }
    }
    array[indexOfMinimum] = array[firstElementIndex];
    array[firstElementIndex] = minimum;
    selectionSort(array, arraySize, firstElementIndex + 1); 
}
