#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <limits>

template <typename T>
T 
recursiveMinimum(const T array[], const size_t sizeOfArray, T minimum = std::numeric_limits<T>::max()) 
{
    if (0 == sizeOfArray) {
        return minimum;
    }
    if (minimum > array[sizeOfArray]) {
        minimum = array[sizeOfArray];
    }
    return recursiveMinimum(array, sizeOfArray - 1, minimum);
}

template <typename T>
void 
inputNumericArray(T array[], const size_t sizeOfArray) 
{
    for (size_t index = 0; index < sizeOfArray; ++index) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "element " << index + 1 << ": ";
        }
        std::cin >> array[index];
    }
}

void 
inputCharArray(char array[], const size_t sizeOfArray) 
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "word: ";
    }
    std::cin >> std::setw(sizeOfArray) >> array;
}

int
main()
{
    const int BUFFER_SIZE = 10;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Insert integers array size and elements(size must be <= 10).\n" << "size: ";
    } 
    size_t integersArraySize;
    std::cin >> integersArraySize;
    if (integersArraySize > BUFFER_SIZE) {
        std::cerr << "Error 1: size must be less or equal than 10.\n";
        return 1;
    }
    int integersArray[BUFFER_SIZE];
    inputNumericArray(integersArray, integersArraySize);
    std::cout << "Minimum is: " << recursiveMinimum(integersArray, integersArraySize - 1);
    std::cout << std::endl;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Insert floating numbers array size and elements(size must be <= 10).\n" << "size: ";
    } 
    size_t floatingArraySize;
    std::cin >> floatingArraySize;
    if (floatingArraySize > BUFFER_SIZE) {
        std::cerr << "Error 1: size must be less or equal than 10.\n";
        return 1;
    }
    double floatingArray[BUFFER_SIZE];
    inputNumericArray(floatingArray, floatingArraySize);
    std::cout << "Minimum is: " << recursiveMinimum(floatingArray, floatingArraySize - 1); 
    std::cout << std::endl;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Insert characters array size and elements(size must be <= 10).\n" << "size: ";
    } 
    size_t charactersArraySize;
    std::cin >> charactersArraySize;
    if (charactersArraySize > BUFFER_SIZE) {
        std::cerr << "Error 1: size must be less or equal than 10.\n";
        return 1;
    }
    char charactersArray[BUFFER_SIZE];
    inputCharArray(charactersArray, charactersArraySize);
    std::cout << "Minimum is: " << recursiveMinimum(charactersArray, charactersArraySize - 2);
    std::cout << std::endl;
    return 0;
}
