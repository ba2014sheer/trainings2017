Question!!!.
Consider a 3-by-4 integer array t .
a. Write a declaration for t .
b. How many rows does t have?
c. How many columns does t have?
d. How many elements does t have?
e. Write the names of all the elements in row 1 of t .
f. Write the names of all the elements in colum n 2 of t .
g. Write a single statement that sets the element of t in row 1 and column 2 to zero.
h. Write a series of statements that initialize each element of t to zero. Do not use a loop.
i. Write a nested for statement that initializes each element of t to zero.
j. Write a statement that inputs the values for the elements of t from the terminal.
k. Write a series of statements that determine and print the smallest value in array t .
l. Write a statement that displays the elements in row 0 of t .
m. Write a statement that totals the elements in column 3 of t .
n. Write a series of statements that prints the array t in neat, tabular form at. List the column subscripts as 
headings across the top and list the row subscripts at the left of each row.

Answer!!!.
a. const int HEIGHT = 3;
   const int LENGTH = 4;
   int t[HEIGHT][LENGTH];
---------------------------------------------------------------------------------------------------------------
b. 3
---------------------------------------------------------------------------------------------------------------
c. 4
---------------------------------------------------------------------------------------------------------------
d. 12
---------------------------------------------------------------------------------------------------------------
e.t[1][0], t[1][1], t[1][2], t[1][3].
---------------------------------------------------------------------------------------------------------------
f.t[0][2], t[1][2], t[2][2].
---------------------------------------------------------------------------------------------------------------
g.t[0][1] = 0.
---------------------------------------------------------------------------------------------------------------
h.t[0][0] = 0;
  t[0][1] = 0;
  t[0][2] = 0;
  t[0][3] = 0;
  t[1][0] = 0;
  t[1][1] = 0;
  t[1][2] = 0;
  t[1][3] = 0;
  t[2][0] = 0;
  t[2][1] = 0;
  t[2][2] = 0;
  t[2][3] = 0;
---------------------------------------------------------------------------------------------------------------
i. for (int row = 0; row < HEIGHT; ++row) {
       for (int column = 0; column < LENGTH; ++column) {
           t[row][column] = 0;
       }
   }
---------------------------------------------------------------------------------------------------------------
j. for (int row = 0; row < HEIGHT; ++row) {
       for (int column = 0; column < LENGTH; ++column) {
           std::cin >> t[row][column];
       }
   }
---------------------------------------------------------------------------------------------------------------
k. 
   int min = t[0][0];
   for (int row = 0; row < HEIGHT; ++row) {
       for (int column = 0; column < LENGTH; ++column) {
           if (min >= t[row][column]) {
               min = t[row][column];
           }
       }
   }
   std::cout << min << std::endl;
---------------------------------------------------------------------------------------------------------------
l. for (int column = 0; column <= LENGTH; ++column) {
       std::cout << " " << t[0][column];
   }
   std::cout << std::endl;
---------------------------------------------------------------------------------------------------------------
m. int sum = 0;
   for (int row = 0; row < HEIGHT; ++row) {
       sum += t[row][3];
   }
---------------------------------------------------------------------------------------------------------------
n. for (int columnNumber = 1; columnNumber <= LENGTH; ++columnNumber) {
       std::cout << columnNumber << "  ";
   }
   std::cout << "\n" << std::endl;
   for (int row = 0; row < HEIGHT; ++row) {
       std::cout << row + 1 << "   ";
       for (int column = 0; column < LENGTH; ++column) {
           std::cout << t[row][column] << "  ";
       }
       std::cout << "\n" << std::endl;
   }
---------------------------------------------------------------------------------------------------------------

