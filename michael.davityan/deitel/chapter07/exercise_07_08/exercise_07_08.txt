Question!!!.
Write C++ statements to accomplish each of the following:
a. Display the value of element 6 of character array f .
b. Input a value into element 4 of one-dimensional floating-point array b .
c. Initialize each of the 5 elements of one-dimensional integer array g to 8 .
d. Total and print the elements of floating-point array c of 100 elements.
e. Copy array a into the first portion of array b . Assume double a[ 11 ], b[ 34 ];
f. Determine and print the smallest and largest values contained in 99-element floating-point array w .

Answer!!!.
-----------------------------------------------------------------------------------------------
a. std::cout << f[6] << std::endl;
-----------------------------------------------------------------------------------------------
b. std::cin >> b[4];
-----------------------------------------------------------------------------------------------
c. for (int index = 0; index < 5; ++index) {
       g[index] = 8;
   }
-----------------------------------------------------------------------------------------------
d. double sum = 0;
   for (int index = 0; index < 100; ++index) {
       sum += g[index];
   }
   std::cout << sum << std::endl;
-----------------------------------------------------------------------------------------------
e. for (int index = 0; index < 11; ++index) {
       b[index] = a[index];
   }
-----------------------------------------------------------------------------------------------
f. There is a two metod to determine and print the smallest and largest values.
===============================================================================================
//// first bubble sort!!!!.
   double hold;
   for (int pass = 0; pass < 98; ++pass) {
       for (int index = 0; index < 98; ++index) {
           if (w[index] > w[index + 1]) {
               hold = w[index];
               w[index] = w[index + 1];
               w[index + 1] = hold;
           }
       }
   } 
   std::cout << "minimum is " << w[0] << "\n";
   std::cout << "maximum is " << w[98] << std::endl;

===============================================================================================
//// second is the standart method.
   int min = w[0];
   int max = w[0];
   for (int index = 0; index < 99; ++index) {
       if (min >= w[index]) {
           min = w[index];
       }
       if (max <= w[index]) {
           max = w[index];
       }
   }
   std::cout << "minimum is " << min << "\n";
   std::cout << "maximum is " << max << std::endl;

   I think this method is much faster than the bubble sorting!!!.
------------------------------------------------------------------------------------------------
