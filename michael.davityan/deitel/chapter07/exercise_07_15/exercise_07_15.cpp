#include <iostream>
#include <unistd.h>

int
main()
{
    const int ARRAY_SIZE = 19;
    int numbersHoldingArray[ARRAY_SIZE] = {0};
    for (int count = 0; count <= ARRAY_SIZE; ++count) {
        int number;
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Input number" << count + 1 << ": ";
        }
        std::cin >> number;
        if (number < 10 || number > 100) {
            std::cerr << "Error 1: Input number is wrong. It must be in range [10, 100].\n";
            return 1;
        }
        bool duplicateDetectVariable = true;
        for (int arrayIndex = 0; arrayIndex < count; ++arrayIndex) {
            if (numbersHoldingArray[arrayIndex] == number) {
                duplicateDetectVariable = false;
                break;
            }
        }
        if (duplicateDetectVariable) {
            numbersHoldingArray[count] = number;
            std::cout << number << std::endl;
        }
    } 
    return 0;
}
