#include <iostream>
#include <cstdlib>
#include <ctime>
#include <unistd.h>

void printMessage(bool answerStatus);
void printPassMessage();
void printNotPassMessage();

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::srand(std::time(0));
    } 
    bool answerStatus = true;
    int number1 = 0;
    int number2 = 0;
    while (true) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "How much is  "; 
        }
        if (answerStatus) {
            number1 = 1 + std::rand() % 10;
            number2 = 1 + std::rand() % 10;
        }
        if (::isatty(STDIN_FILENO)) {
            std::cout << number1 << " * " << number2 << "?: "; 
        }
        int answer;
        std::cin >> answer;
        if (-1 == answer) {
            break;
        }
        answerStatus = (answer == number1 * number2);
        printMessage(answerStatus);
    }
    return 0;
}

void
printMessage(bool answerStatus) 
{
    if (answerStatus) {
        printPassMessage();
    } else {
        printNotPassMessage();
    }
}

void 
printPassMessage()
{
    std::cout << "Very good!\n"; 
}

void 
printNotPassMessage()
{
    std::cout << "No. Please try again.: ";
}
