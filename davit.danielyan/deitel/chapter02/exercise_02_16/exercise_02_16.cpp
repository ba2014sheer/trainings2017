#include <iostream>

int
main()
{
    int a, b; 
    std::cout << "Enter two numbers: ";
    std::cin >> a >> b;
	        
    if (0 == b) {
        std::cout << "Error 1: Division is 0" << std::endl;   
        return 1;
    } 
	    
    std::cout << "a + b = " << a + b << std::endl;
    std::cout << "a - b = " << a - b << std::endl;
    std::cout << "a * b = " << a * b << std::endl;
    std::cout << "a / b = " << a / b << std::endl; 
					    
    return 0;
}

