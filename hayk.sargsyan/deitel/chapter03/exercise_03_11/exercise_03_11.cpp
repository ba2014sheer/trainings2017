#include <iostream>
#include "GradeBook.hpp"

int 
main()
{
    GradeBook gradeBook1("Lesson 1. Vocal lesson", "Robert Plant");
    GradeBook gradeBook2("Lesson 2. Some guitar pentatonic skills", "Jimmy Page");
    
    gradeBook1.displayMessage();
    gradeBook2.displayMessage();

    return 0;
}

