#include <string>

class Employee
{
public:
    Employee(std::string name, std::string surname, int salary);
    void setName(std::string name);
    void setSurname(std::string surname);
    void setSalary(int salary);
    std::string getName();
    std::string getSurname();
    int getSalary();
private:
    std::string name_;
    std::string surname_;
    int salary_;
};

