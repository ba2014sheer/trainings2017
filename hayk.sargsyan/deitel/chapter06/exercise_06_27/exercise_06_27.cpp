#include <iostream>
#include <iomanip>

float
celsius(const int fahrenheit) 
{
    return (fahrenheit - 32) * 5 / 9;
}

float
fahrenheit(const int celsius) 
{
    return celsius * 9 / 5 + 32;
}

int
main()
{
    for (int counter = 1; counter <= 100; ++counter) {
        std::cout << "Celsius: " << counter << " Fahrenheit: "<< fahrenheit(counter) << std::endl;
    }
    
    std::cout << std::endl;

    for (int counter = 32; counter <= 212; ++counter) {
        std::cout << "Fahrenheit: " << counter << " Celsius: "<< celsius(counter) << std::endl;
    }

    return 0;
}
