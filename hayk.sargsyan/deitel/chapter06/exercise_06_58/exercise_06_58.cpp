#include <iostream>

template <typename T>

T
min(const T variable1, const T variable2)
{
    return variable1 < variable2 ? variable1 : variable2;
}

int
main()
{
    std::cout << "The minimal of 3 and 4 is " << min(3, 4) << std::endl;
    std::cout << "The minimal of 3.457 and 0.1 is " << min(3.457, 0.1) << std::endl;
    std::cout << "The minimal of 'a' and 'b' is " << min('a', 'b') << std::endl;

    return 0;
}
