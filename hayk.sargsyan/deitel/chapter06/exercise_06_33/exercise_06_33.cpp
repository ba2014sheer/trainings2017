#include <iostream>
#include <assert.h>

int 
grade(const int points)
{
    assert(points >= 0);
    return (points - 50) > 0 ? (points - 50) / 10 : 0;
}

int
main()
{
    int points;

    std::cout << "Please enter points: ";
    std::cin >> points;

    if (points < 0) {
        std::cerr << "Error 1: Points can't be negative" << std::endl;
        return 1;
    }

    std::cout << "For " << points << " points grade is " << grade(points) << std::endl;

    return 0;
}
