#include <iostream>

int
main() 
{
    int number;
    std::cin >> number;
    int initialNumber = number; ///To store the initial value
    int reverse = 0;
    while (number != 0) {
        int digit = number % 10;
        reverse = (reverse * 10) + digit;
        number = number / 10;
    }	

    if (reverse == initialNumber) {
        std::cout << "The number is polindrome" << std::endl;
    } else {
        std::cout << "The number is not polindrome" << std::endl;
    }
    return 0;
}
