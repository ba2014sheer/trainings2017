#include <iostream>

int 
main()
{
    int counter;

    std::cout << "Enter the count of numbers: ";
    std::cin >> counter;

    if (counter <= 0) {
        std::cerr << "Error 1: Non positive count." << std::endl;
        return 1;
    }
    
    int sum = 0;
    std::cout << "Enter numbers: ";
    
    for (int i = 1; i <= counter; i++) {
        int number;        
        std::cin >> number;
        sum += number;
    }

    std::cout << "Sum is " << sum << std::endl;
    return 0;
}

