#include <iostream>

int 
main()
{
    int count;

    std::cout << "Enter the count of numbers: ";
    std::cin >> count;

    if(count <= 0) {
        std::cerr << "Error 1: Non positive count." << std::endl;
        return 1;
    }

    int smallest = 2147483647;
    for (int i = 1; i < count; ++i) {
        int number;
        std::cin >> number;

        if (number < smallest) {
            smallest = number;
        } 
    
    }

    std::cout << "Smallest number is " << smallest << std::endl;
    return 0;
}
