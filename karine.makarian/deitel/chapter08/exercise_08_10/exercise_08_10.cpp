long int value1 = 200000;
long int value2;

///a.
long int *longptr;

///b.
longptr = &value1;

///c.
std::cout << *longptr;

///d.
value2 = *longptr;

///e.
std::cout << value2;

///f.
std::cout << &value2;

///g.
std::cout << longptr; longptr contains the address of value1.
