a)
float cube( float ); 

double cube( float number ) ///return type must be the same as in prototype...float
{
    return number * number * number;
}

b)register auto int x = 7; ////variable type can't have two memory specificators, either register, or auto.
every local variable automaticaly have specificator auto... so register int x = 7;

c) int randomNumber = srand(); ////srand() can not be assigned to variable because srand() 
function with argument generates a new number randomness for 
rand() function, after each running of program. rand() is much better 


d)
float у = 123.45678;
int x;
x = y; ///x = 123
std::cout << static_cast< float > ( x ) << std::endl; ///output will be 123.0...not the initial value

e)
double square( double number )
{
    double number ///this is excessive and also we can't declare a variable with an existing name
    return number * number;
}

f)
int sum( int n )
{
    if ( n == 0 ) /// (0 ==n)
        return 0;
    else
        return n + sum( n ) ; /// return n + sum(n-1)
}
