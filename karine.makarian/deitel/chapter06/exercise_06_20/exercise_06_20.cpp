#include <iostream>
#include <assert.h>

bool 
multiple(const int number1, const int number2)
{
    assert(number2 != 0);
    return 0 == number1 % number2;
}

int
main()
{
    while (true) {
        int number1;
        int number2;

        std::cout << "Enter a pair of numbers (-1 to exit): ";
        std::cin >> number1 >> number2;

        if (-1 == number1 || -1 == number2) {
            break;
        }

        if (0 == number2) {
            std::cerr << "Error 1: Second number can't be zero." << std::endl;
            return 1;
        }

        if (multiple(number1, number2)) {
            std::cout << number1 << " is the multiple of " << number2 << std::endl;
        } else {
            std::cout << number1 << " is not the multiple of " << number2 << std::endl;
        }
    }

    return 0;
}

